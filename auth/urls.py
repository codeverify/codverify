from rest_framework import routers
from django.urls import path
from django.conf.urls import url, include
from ..views.loginview import LoginAPIView
from ..views.newpasswordview import PasswordTokenCheckAPI
from ..views.specializationview import SpecializationView
from ..views.topicview import FeedView
from ..views.topicimageview import TopicImageView
from rest_framework import routers

router = routers.DefaultRouter()

router.register('specialization', SpecializationView, 'specialization')

urlpatterns = [
    path('login/', LoginAPIView.as_view(), name="login"),
    path('password-reset/<uidb64>/<token>/',
         PasswordTokenCheckAPI.as_view(), name='password-reset-confirm'),
    path('topic/', FeedView.as_view(), name="topic"),
    path('topicimage/', TopicImageView.as_view(), name="topicimage"),


    ]