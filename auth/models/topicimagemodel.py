from django.db import models

class Image(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    topic_id = models.ForeignKey(Topics,on_delete=models.CASCADE,related_name="topic_image")
    image = models.ImageField(blank=True, null=True, upload_to=get_image_path_topic)
