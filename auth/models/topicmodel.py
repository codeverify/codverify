from django.db import models

class Topics(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    AUDIENCE_CHOICES = (
        ('doctor', 'doctor'),
        ('general', 'general')
    )
    topic_audience = models.CharField(max_length=20,choices=AUDIENCE_CHOICES)
    FORMAT_CHOICES = (
        ('1', '1'),
        ('2', '2'),
        ('3', '3')
    )
    format = models.CharField(max_length=2,choices=FORMAT_CHOICES)
    category_id = models.ForeignKey(Categoeries,on_delete=models.CASCADE,related_name="topic_category")
    title = models.CharField(max_length=255)
    description = models.CharField(max_length=1000)
    TYPE_CHOICES = (
        ('pdf', 'pdf'),
        ('external', 'external')
    )
    deliverytype = models.CharField(max_length=20,choices=TYPE_CHOICES,default='external',blank=True,null=True)
    pdf = models.FileField(blank=True,null=True,upload_to=get_pdf_path)
    pdfsecond = models.FileField(blank=True,null=True,upload_to="pdfsecond")
    source_url=models.CharField(max_length=255,blank=True,null=True)
    external_url = models.CharField(max_length=255,blank=True,null=True)
    MEDIA_CHOICES =(
        ('image', 'image'),
        ('video', 'video')
    )
    media_type = models.CharField(max_length=20,choices=MEDIA_CHOICES,blank=True,null=True)
    video_url = models.CharField(max_length=1000,blank=True,null=True)
    publishingtime = models.DateTimeField()
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.title
    
    class Meta:
        db_table = 'Topics'