from  ..models.topicmodel import Topics 
from ..serializers.topicserializer import TopicSeriaizer
from rest_framework.response import Response
from rest_framework import viewsets
from rest_framework import status,permissions
from rest_framework import pagination
from rest_framework.views import APIView
from rest_framework.permissions import IsAuthenticated,AllowAny

class TwentyPagination(pagination.PageNumberPagination): 
    '''
        class for pagination
    '''
    page_size = 4
    def funA(self,x):
        TwentyPagination.page_size = x

class FeedView(APIView):
    queryset = Topics.objects.all().order_by('-created_at')
    serializer_class = TopicSeriaizer
    permission_class =(IsAuthenticated,)
    pagination_class  = TwentyPagination
    def post(self,request):
                '''
                    to display the add between feeds as decided by user
                '''
                add = AddSetting.objects.latest('id')
                ob1 = TwentyPagination()

                '''
                     to change pagination size
                '''
                ob1.funA(add.addaftertopic)
                '''
                    check the id of add from client side
                '''
                if  request.data['addid']:
                    addid = request.data['addid']
                else :
                    addid = 0
                '''
                    feeds which needed to show specify user only
                '''
                userCategory = UserCategory.objects.filter(user_id=self.request.user)
                idscat = list(userCategory.category_id.id for userCategory in userCategory)
                userspec = UserSubSpecialization.objects.filter(user_spec_id__user_id=self.request.user)
                idsspec = list(userspec.sub_spec_id.id for userspec in userspec)
                queryset = Topics.objects.filter(topic_subspec__subspec_id__id__in=idsspec).filter(category_id__id__in=idscat,publishingtime__lt=datetime.now()).order_by('-publishingtime').distinct()
                ff = FooFilter()
                filtered_queryset = ff.filter_queryset(request, queryset, self)
                '''
                    if feeds for user exists
                '''
                if filtered_queryset.exists():
                    page = self.paginate_queryset(filtered_queryset)
                    if page is not None:
                        serializer = self.serializer_class(page, many=True,context = {'request':self.request})
                        data = {}
                        data['feeds'] = serializer.data
                        data['page_size'] = add.addaftertopic
                        try :

                            '''
                              append add in between feed
                            '''
                            add = AllUserAdd.objects.filter(active=True).order_by('-id')[addid]
                            addid= addid+1                         
                            adddata={
                            'title':add.title,
                            'addimage':"https://"+str(get_current_site(request).domain)+"/media/"+str(add.addimage),
                            'url':add.url,
                            'isadd':True
                            }
                            data['feeds'].append(adddata)
                        except Exception as e:
                            '''
                                add list id finished show add from begining
                            '''
                            addcount = AllUserAdd.objects.filter(active=True).count()
                            if addcount>0:
                                add = AllUserAdd.objects.filter(active=True).order_by('-id')[0]
                                addid = 0                         
                                adddata={
                                'title':add.title,
                                'addimage':"https://"+str(get_current_site(request).domain)+"/media/"+str(add.addimage),
                                'url':add.url,
                                'isadd':True
                                }
                                data['feeds'].append(adddata)
                            else:
                                '''
                                    no add to display
                                '''
                                adddata ={}
                                addid=0
                        data['addid'] = addid
                        return self.get_paginated_response(data)
                return Response({"results":{"feeds":[]}},status=status.HTTP_200_OK)
    @property
    def paginator(self):
            """
            The paginator instance associated with the view, or `None`.
            """
            if not hasattr(self, '_paginator'):
                if self.pagination_class is None:
                    self._paginator = None
                else:
                    self._paginator = self.pagination_class()
            return self._paginator

    def paginate_queryset(self, queryset):
            """
            Return a single page of results, or `None` if pagination is disabled.
            """
            if self.paginator is None:
                return None
            return self.paginator.paginate_queryset(queryset, self.request, view=self)

    def get_paginated_response(self, data):
            """
            Return a paginated style `Response` object for the given output data.
            """
            assert self.paginator is not None
            return self.paginator.get_paginated_response(data)

