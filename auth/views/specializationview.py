from  ..models.specializationmodel import Specialization
from ..serializers.specializationseriallizer import GetSpecializationseriallizer
from rest_framework.response import Response
from rest_framework import viewsets
from rest_framework import status,permissions
from rest_framework.permissions import IsAuthenticated,AllowAny

class SpecializationView(viewsets.ModelViewSet):
    '''
        api for user specializations
    '''
    queryset = Specialization.objects.all().order_by('name')
    serializer_class = GetSpecializationseriallizer
    permission_classes = (IsAuthenticated,)

    '''
        override create method to check user already has specialization
    '''
    def create(self, request):
        name = request.data['name']
        if Specialization.objects.filter(name__iexact=name):
            status_code = status.HTTP_400_BAD_REQUEST
            response = {
                'success': 'false',
                'status code': status.HTTP_400_BAD_REQUEST,
                'message': 'specialization already exists with this name'
                }
            return Response(response, status=status_code)
        serializer = GetSpecializationseriallizer(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(serializer.data)
    '''
        override create method to check user already has specialization
    '''
    def update(self, request, *args, **kwargs):
        name = request.data['name']
        '''
            get current id exclude the current name
        '''
        idpk=kwargs['pk']
        if Specialization.objects.filter(name__iexact=name).exclude(id=idpk):
            status_code = status.HTTP_400_BAD_REQUEST
            response = {
                'success': 'false',
                'status code': status.HTTP_400_BAD_REQUEST,
                'message': 'specialization already exists with this name'
                }
            return Response(response, status=status_code)
        instance = self.get_object()
        serializer = self.get_serializer(instance, data=request.data, partial=partial)
        serializer.is_valid(raise_exception=True)
        self.perform_update(serializer)
        return Response(serializer.data)