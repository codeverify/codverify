from django.shortcuts import render
from ..serializers.loginserializer import (LoginSerializer)
from rest_framework.response import Response
from rest_framework import generics, status, views, permissions




class LoginAPIView(generics.GenericAPIView):
	'''
		api to login user with otp
	'''
    serializer_class = LoginSerializer
    def post(self, request):
        now = datetime.datetime.utcnow()
        '''
        	get phone from requuest and check user exist
        '''
        try:
            user = User.objects.get(phone=request.data['phone'])
        except User.DoesNotExist:
            response = {
                'success' : 'False',
                'status code' : status_code,
                'message': 'Phone does not exists!',
                }
            return Response(response, status=status.HTTP_404_NOT_FOUND)
        otpvallid = user.optvalid
           
        '''
        	check whether the current time is greater than the opt epiration time
        '''
        if now.strftime("%Y-%m-%d %H:%M:%S") > otpvallid.strftime("%Y-%m-%d %H:%M:%S"):
            status_code = status.HTTP_400_BAD_REQUEST
            response = {
                'success' : 'False',
                'status code' : status_code,
                'message': 'otp expired!',
                }
            return Response(response, status=status.HTTP_400_BAD_REQUEST)
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        status_code = status.HTTP_200_OK
        response = {
        'success' : 'True',
        'status code' : status_code,
        'message': 'user logged in',
        'data':serializer.data
        }
        return Response(response, status=status.HTTP_200_OK)