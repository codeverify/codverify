from  ..models.topicimagemodel import Image
from ..serializers.topicimageserializer import TopicImageSerializer
from rest_framework.response import Response
from rest_framework import viewsets
from rest_framework import status,permissions
from rest_framework import pagination
from rest_framework.views import APIView
from rest_framework.permissions import IsAuthenticated,AllowAny
from rest_framework.parsers import MultiPartParser, FormParser

'''
    dict of image and topic id
'''

def modify_input_for_multiple_files(topic_id, image):
    dict = {}
    dict['topic_id'] = topic_id

    dict['image'] = image
    return dict

class TopicImageView(APIView):
    '''
        to save multiple topic images
    '''
    permission_classes = (IsAuthenticated,)
    parser_classes = (MultiPartParser, FormParser)
    @csrf_exempt
    def post(self, request, *args, **kwargs):
        topic_id = request.data['topic_id']
        images = dict((request.data).lists())['image']
        flag = 1
        arr = []
        '''
            create imagename and topic id dict 
        '''
        for img_name in images:
            modified_data = modify_input_for_multiple_files(topic_id,
                                                            img_name)
            file_serializer = TopicImageSerializer(data=modified_data,context={"request": request})
            if file_serializer.is_valid():
                file_serializer.save()
                arr.append(file_serializer.data)
            else:
                flag = 0

        if flag == 1:
            return Response(arr, status=status.HTTP_201_CREATED)
        else:
            return Response(arr, status=status.HTTP_400_BAD_REQUEST)