from django.db import models
from ..models.topicimagemodel import Image

class TopicImageSerializer(serializers.ModelSerializer):
    # image = serializers.ListField(child=serializers.ImageField(max_length=100000,allow_empty_file=False,use_url=False))
    class Meta:
        model = Image
        fields = ('image','topic_id')