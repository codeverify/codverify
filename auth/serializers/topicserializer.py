from rest_framework import serializers
from ..models.topicmodel import Topics


class TopicsubSpecializationSerializer(serializers.ModelSerializer):
    id = serializers.ReadOnlyField()
    class Meta:
        model = TopicSubSpecialization
        fields = ['id','subspec_id']

    def to_representation(self, instance):
        response = super().to_representation(instance)
        response['subspec_id'] = GetSubspecializationSerializer(instance.subspec_id).data
        return response

class TopicSeriaizer(serializers.ModelSerializer):
    topic_image = ImageSerializer(many=True, read_only=True)
    favourite_topic = TopicFavouriteserializer(many=True,read_only=True)
    pdf =serializers.FileField(max_length=None,use_url=True, allow_null=True, required=False)
    pdfsecond = serializers.FileField(max_length=None,use_url=True, allow_null=True, required=False)
    topic_subspec = TopicsubSpecializationSerializer(many=True)
    published = serializers.SerializerMethodField()
    externalurltype = serializers.SerializerMethodField()
    isadd = serializers.SerializerMethodField()
    class Meta:
        model = Topics
        fields = '__all__'

    '''
        function to give add as false
    '''
    def get_isadd(self,obj):
        return False

    '''
        check the url is pdf or external
    '''
    def get_externalurltype(self,obj):
        etype = ""
        formattype = obj.format
        url = obj.external_url
        if formattype=='1':
            etype='pdf'
        else:
            if  url=="":
                etype=""
            elif str(url)[-4:]=='.pdf':
                etype="pdf"
            else:
                etype="external"
        return etype

    '''
        check the feed is published
    '''
    def get_published(self, obj):
        now = datetime.utcnow()
        publishtime = obj.publishingtime
        if publishtime.strftime("%Y-%m-%d %H:%M:%S")<=now.strftime("%Y-%m-%d %H:%M:%S"):
            published = 1
        else:
            published = 0
        return published

    def to_representation(self, instance):
        response = super().to_representation(instance)
        response['category_id'] = CategorySerializer(instance.category_id).data
        return response
    '''
        create 
    '''
    def create(self, validated_data):
        topic_spec_data = validated_data.pop('topic_subspec')
        if 'email' in validated_data:
            email = validated_data['email']
            del validated_data['email']
            author = User.objects.get(email=email)
            topic = Topics.objects.create(author=author,**validated_data)
        else:
            topic = Topics.objects.create(**validated_data)
        for data in topic_spec_data:
            topic_spec =TopicSubSpecialization.objects.create(topic_id = topic, **data)
        return topic