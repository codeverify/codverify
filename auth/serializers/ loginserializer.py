from rest_framework import serializers
from ..models.usermodel import User


class LoginSerializer(serializers.ModelSerializer):
    phone = serializers.CharField(max_length=20,min_length=10)
    otp = serializers.CharField(max_length=20)
    tokens = serializers.SerializerMethodField()
    speccount = serializers.SerializerMethodField()
    categorycount = serializers.SerializerMethodField()

    '''
    	this function returns the count of categories selected by user
    '''
    def get_categorycount(self,obj):
        userobj = User.objects.get(phone = obj['phone'])
        userid = userobj.id
        return UserCategory.objects.filter(user_id=userid).count()

    '''
    	this function returns the count of specializations selected by user
    '''
    def get_speccount(self,obj):
        userobj = User.objects.get(phone = obj['phone'])
        userid = userobj.id
        return UserSpecialization.objects.filter(user_id=userid).count()
    '''
    	this function returns the access token and refresh token to login
    '''
    def get_tokens(self, obj):
        user = User.objects.get(phone=obj['phone'])

        return {
            'refresh': user.tokens()['refresh'],
            'access': user.tokens()['access']
        }

    class Meta:
        model = User
        fields = ['tokens','phone','otp','speccount','categorycount']


    def validate(self, attrs):
        phone = attrs.get('phone','')
        otp = attrs.get('otp','')
        password = os.environ.get('SOCIAL_SECRET')
        '''
        	get the user details from phone number  
        '''
        filtered_user_by_email = User.objects.filter(phone=phone)
        if not filtered_user_by_email:
            raise AuthenticationFailed('user not found!')
        for filtered_user_by_email in filtered_user_by_email:
            email  = filtered_user_by_email.email
            user_otp = filtered_user_by_email.otp
        if int(otp) != int(user_otp):
            raise AuthenticationFailed('Invalid otp, try again')

        '''
        	make phone valid if otp is correct
        '''
        userobj = User.objects.get(phone = phone)
        userobj.phone_verified = True
        userobj.is_verified = True
        userobj.save()
        '''
        authenticate user
        '''
        user = auth.authenticate(email=email, password=password)
        if not email:
            raise AuthenticationFailed('Invalid credentials, try again')
        if not user:
            raise AuthenticationFailed('Invalid credentials, try again')
        if not user.is_active:
            raise AuthenticationFailed('Account disabled, contact admin')
        if not user.is_verified:
            raise AuthenticationFailed('user not verified')
        if not user.phone_verified:
            raise AuthenticationFailed('User not verified')

        return {
            'email': user.email,
            'username': user.username,
            'tokens': user.tokens,
            'phone':user.phone,
            'otp':user.otp
        }

        return super().validate(attrs)
