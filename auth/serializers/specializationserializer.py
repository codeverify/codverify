from rest_framework import serializers

class GetSpecializationseriallizer(serializers.ModelSerializer):
    id = serializers.ReadOnlyField()
    class Meta:
        model = Specialization
        fields = '__all__'
